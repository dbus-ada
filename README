D_Bus/Ada
=========

The D_Bus/Ada library provides an Ada binding to the D-Bus message bus used for
inter-process communication on most modern Linux desktop systems.


Licence
-------
--------------------------------------------------------------------------------
Copyright (C) 2011-2024 Reto Buerki <reet@codelabs.ch>. Free use of this
software is granted under the terms of the GNAT Modified General Public License
(GMGPL).
--------------------------------------------------------------------------------


Download
--------

Release version
~~~~~~~~~~~~~~~
The current release version of D_Bus/Ada is available at
https://www.codelabs.ch/download/.

Verify a Release
~~~~~~~~~~~~~~~~
To verify the integrity and authenticity of the distribution tarball type the
following commands:

  $ wget -q https://www.codelabs.ch/keys/DBF6D7E1095FD0D9.asc -O - | gpg --import
  $ gpg --verify libdbusada-{version}.tar.bz2.sig

The key fingerprint of the public key ('0xDBF6D7E1095FD0D9') is:

  Key fingerprint = 298F 4B32 C3C4 1D88 5949  86F3 DBF6 D7E1 095F D0D9

Development version
~~~~~~~~~~~~~~~~~~~
The current development version of D_Bus/Ada is available through its git
repository:

  $ git clone https://git.codelabs.ch/dbus-ada.git

A browsable version of the repository is available here:
https://git.codelabs.ch/?p=dbus-ada.git.


Build
-----
To compile D_Bus/Ada on your system, you need to have the following software
installed:

* GNAT compiler:
  https://www.gnu.org/software/gnat

* Ahven (Test-Framework):
  http://ahven.stronglytyped.org

* D-Bus development files and libraries
  https://www.freedesktop.org/wiki/Software/dbus

* Glib with D-Bus support


Testing
-------
Before you install D_Bus/Ada on your system, you might want to test the library
and verify that everything works as expected. D_Bus/Ada contains an unit test
suite which can be run by entering the following command:

  $ make tests


Installation
------------
To install D_Bus/Ada on your system, type the following:

  $ make PREFIX=/usr/local install

If no `PREFIX` is specified, `$(HOME)/libraries` is used as install destination.


Examples
--------
D_Bus/Ada provides example code to demonstrate the usage of the D_Bus/Ada API.
To build all examples type the following:

  $ make examples

You can start an example application like so: `obj/examples/list_names`.
